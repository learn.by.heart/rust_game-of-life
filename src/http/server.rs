use std::{net::{TcpListener, TcpStream}, io::{BufReader, BufRead, Write}, fs};

use crate::http::{Request, Method, Response, StatusCode};

use super::super::model::{World, ToHtml};

#[derive(Debug)]
pub struct Server {
    address: String
}

impl Server {
    pub fn new(address: String) -> Self {
        Self {address}
    }

    pub fn start(&self) {
        let listener = TcpListener::bind(&self.address).unwrap();

        let mut world = World::new();

        for result in listener.incoming() {
            if result.is_ok() {
                let stream = result.unwrap();
                Server::handle_connection(stream, &mut world);
            } else {
                dbg!("Failed to establish a connection: {}", result.expect_err("connection failed"));
            }
         }
    }

    fn handle_connection(mut stream: TcpStream, world: &mut World) {
        let buf_reader = BufReader::new(&mut stream);

        let request_line = buf_reader.lines().next()
            .filter(|value| value.is_ok())
            .map(|value| value.unwrap())
            .expect("client message");
    
        let result_request = Request::try_from(request_line);
        match result_request {
            Ok(request) => Server::work_on_request(stream, request, world),
            Err(parse_error) => print!("{}", parse_error),
        }
    }

    fn work_on_request(mut stream: TcpStream, request: Request, world: &mut World) {
        //dbg!(&request);

        let response: Response;
        let contents: String;
        if request.method == Method::GET {
            if request.query == "/" {
                response = Response::new(request.protocol, StatusCode::Ok, None);
                contents = fs::read_to_string("src/index.html").unwrap();
                world.init_world();
            } else if request.query == "/next_world" {
                response = Response::new(request.protocol, StatusCode::Ok, None);
                world.calculate_next_world();
                contents = world.to_html();
            } else {
                return;
            }
        } else {
            response = Response::new(request.protocol, StatusCode::NotFound, None);
            contents = fs::read_to_string("src/404.html").unwrap();
        };
    
        let lenght = contents.len();
        let response_complete = format!("{}\r\nContent-Lenght: {lenght}\r\n\r\n{contents}", response.to_string());
        
        stream.write_all(response_complete.as_bytes()).unwrap();
    }
}