use rand::Rng;

const COLUMNS: usize = 50;
const ROWS: usize = 30;

#[derive(Debug)]
pub struct World {
    world: [[u8; COLUMNS]; ROWS],
    population: f64,
}

impl World {
    pub fn new() -> Self {
        Self {world: [[0; COLUMNS]; ROWS], population: 0.3}
    }

    pub fn row_count(&self) -> usize {
         return self.world.len();
    }

    pub fn column_count(&self) -> usize {
        return self.world[0].len();
    }

    pub fn init_world(&mut self) {
        let mut randomizer = rand::thread_rng();
    
        for row in 0..self.world.len() {
            for column in 0..self.world[row].len() {
                let random_value: f64 = randomizer.gen();
                if random_value < self.population {
                    self.world[row][column] = 1;
                }
            }
        }
    }

    /*
    pub fn print_world(&self) {
        println!("\n---");
        self.world.iter().for_each(|row| println!("{:?}", row));
    }
    */

    fn count_neighbours(&self, row: usize, column: usize) -> u8 {
        let row_limit = self.row_count();
        let column_limit = self.column_count();

        let north = if row>0 {self.world[row-1][column]} else {0};
        let north_east = if row>0 && column<column_limit-1 {self.world[row-1][column+1]} else {0};
        let east = if column<column_limit-1 {self.world[row][column+1]} else {0};
        let south_east = if row<row_limit-1 && column<column_limit-1 {self.world[row+1][column+1]} else {0};
        let south = if row<row_limit-1 {self.world[row+1][column]} else {0};
        let south_west = if row<row_limit-1 && column>0 {self.world[row+1][column-1]} else {0};
        let west = if column>0 {self.world[row][column-1]} else {0};
        let north_west = if row>0 && column>0 {self.world[row-1][column-1]} else {0};
    
        return north + north_east + east + south_east + south + south_west + west + north_west;
    }

    fn calculate_life_of_cell_for_next_world(center: u8, neighbours: u8) -> u8 {
        // creation of life
        if center == 0 && neighbours == 3 {
            return 1;
        }
    
        // life goes on
        if center != 0 && (neighbours == 2 || neighbours == 3) {
            return 1;
        }
    
        // life vanishes
        return 0;
    }

    pub fn calculate_next_world(&mut self) {
        // calculate the next world
        let mut next_world =  [[0; COLUMNS]; ROWS];

        for row in 0..self.row_count() {
            for column in 0..self.column_count() {
                let center = self.world[row][column];
                let neighbours = self.count_neighbours(row, column);
    
                next_world[row][column] = Self::calculate_life_of_cell_for_next_world(center, neighbours);
            }
        }
        
        // update our world with the calculated next world
        for row in 0..self.row_count() {
            for column in 0..self.column_count() {
                self.world[row][column] = next_world[row][column];
            }
        }
    }
}

pub trait ToHtml {
    fn to_html(&self) -> String;
}

impl ToHtml for World {
    fn to_html(&self) -> String {
        let mut html = String::from("");

        html.push_str("<table style=\"border-spacing: 1px; background-color:#DCDCDC\">");

        for row in 0..self.row_count() {
            html.push_str("<tr>");
            for column in 0..self.column_count() {
                let color;
                if self.world[row][column] == 1 {
                    color = "background-color:green";
                } else {
                    color = "background-color:black";
                };
                html.push_str("<td style=\"height:10px; width:10px;");
                html.push_str(color);
                html.push_str("\"></td>");
            }
            html.push_str("</tr>");
        }

        html.push_str("</table>");

        return html;
    }
}

#[test]
fn test_world_creation_of_life() {
    let mut test_world = World{world: [[0; COLUMNS]; ROWS], population: 0.3};

    test_world.world[0][0] = 0;
    test_world.world[1][0] = 0;
    test_world.world[2][0] = 0;
    test_world.world[0][1] = 1;
    test_world.world[1][1] = 0;
    test_world.world[2][1] = 0;
    test_world.world[0][2] = 0;
    test_world.world[1][2] = 1;
    test_world.world[2][2] = 1;

    assert_eq!(3, test_world.count_neighbours(1, 1));

    test_world.calculate_next_world();

    assert_eq!(1, test_world.world[1][1]);
}

#[test]
fn test_world_vanish_because_of_loneliness() {
    let mut test_world = World{world: [[0; COLUMNS]; ROWS], population: 0.3};

    test_world.world[0][0] = 0;
    test_world.world[1][0] = 0;
    test_world.world[2][0] = 0;
    test_world.world[0][1] = 0;
    test_world.world[1][1] = 1;
    test_world.world[2][1] = 0;
    test_world.world[0][2] = 0;
    test_world.world[1][2] = 0;
    test_world.world[2][2] = 1;

    assert_eq!(1, test_world.count_neighbours(1, 1));

    test_world.calculate_next_world();

    assert_eq!(0, test_world.world[1][1]);
}

#[test]
fn test_world_life_goes_on() {
    let mut test_world = World{world: [[0; COLUMNS]; ROWS], population: 0.3};

    test_world.world[0][0] = 0;
    test_world.world[1][0] = 1;
    test_world.world[2][0] = 0;
    test_world.world[0][1] = 1;
    test_world.world[1][1] = 1;
    test_world.world[2][1] = 0;
    test_world.world[0][2] = 0;
    test_world.world[1][2] = 0;
    test_world.world[2][2] = 1;

    assert_eq!(3, test_world.count_neighbours(1, 1));

    test_world.calculate_next_world();

    assert_eq!(1, test_world.world[1][1]);
}

#[test]
fn test_world_vanish_because_of_over_population() {
    let mut test_world = World{world: [[0; COLUMNS]; ROWS], population: 0.3};

    test_world.world[0][0] = 1;
    test_world.world[1][0] = 1;
    test_world.world[2][0] = 1;
    test_world.world[0][1] = 0;
    test_world.world[1][1] = 1;
    test_world.world[2][1] = 1;
    test_world.world[0][2] = 1;
    test_world.world[1][2] = 0;
    test_world.world[2][2] = 0;

    assert_eq!(5, test_world.count_neighbours(1, 1));

    test_world.calculate_next_world();

    assert_eq!(0, test_world.world[1][1]);
}
