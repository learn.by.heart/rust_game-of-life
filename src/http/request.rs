use  super::Method;
use std::convert::TryFrom;
use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult, Debug};
use std::str::FromStr;

#[derive(Debug)]
pub struct Request {
    pub method: Method,
    pub query: String,
    pub protocol: String,
}

impl TryFrom<String> for Request {
    type Error = ParseError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let splitted: Vec<&str> = value.split(' ').collect();

        if splitted.len() == 3 {
            let method = Method::from_str(splitted[0]).unwrap();
            let query = splitted[1].to_string();
            let protocol = splitted[2].to_string();

            if protocol != "HTTP/1.1" {
                return Err(ParseError::InvalidProtocol);
            }
            if method == Method::UNKNOWN {
                return Err(ParseError::InvalidMethod);
            }

            return Ok(Request { method, query, protocol });
        } else {
            return Err(ParseError::InvalidRequest);
        }
    }
}


pub enum ParseError {
    InvalidRequest,
    InvalidProtocol,
    InvalidMethod,
}

impl ParseError {
    fn message(&self) -> &str {
        match self {
            Self::InvalidRequest => "Invalid request",
            Self::InvalidProtocol => "Invalid protocol",
            Self::InvalidMethod => "Invalid method",
        }
    }
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Debug for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Error for ParseError {
}