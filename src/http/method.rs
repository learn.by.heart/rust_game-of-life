use std::{fmt::{ Display, Formatter, Result }, str::FromStr};

#[derive(Debug, PartialEq, Eq)]
pub enum Method {
    UNKNOWN,
    
    //The GET method requests a representation of the specified resource. Requests using GET should only retrieve data.
    GET,

    // The HEAD method asks for a response identical to a GET request, but without the response body.
    HEAD,

    // The POST method submits an entity to the specified resource, often causing a change in state or side effects on the server.
    POST,

    // The PUT method replaces all current representations of the target resource with the request payload.
    PUT,

    // The DELETE method deletes the specified resource.
    DELETE,

    // The CONNECT method establishes a tunnel to the server identified by the target resource.
    CONNECT,

    // The OPTIONS method describes the communication options for the target resource.
    OPTIONS,

    // The TRACE method performs a message loop-back test along the path to the target resource.
    TRACE,

    // The PATH method applies partial modifications to a resource.
    PATCH,
}

impl Display for Method {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            Method::GET => write!(f, "GET"),
            Method::HEAD => write!(f, "HEAD"),
            Method::POST => write!(f, "POST"),
            Method::PUT => write!(f, "PUT"),
            Method::DELETE => write!(f, "DELETE"),
            Method::CONNECT => write!(f, "CONNECT"),
            Method::OPTIONS => write!(f, "OPTIONS"),
            Method::TRACE => write!(f, "TRACE"),
            Method::PATCH => write!(f, "PATCH"),
            _ => write!(f, "UNKNOWN"),
        }
    }
}

impl FromStr for Method {
    type Err = String;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        match s {
            "GET" => Ok(Method::GET),
            "HEAD" => Ok(Method::HEAD),
            "POST" => Ok(Method::POST),
            "PUT" => Ok(Method::PUT),
            "DELETE" => Ok(Method::DELETE),
            "CONNECT" => Ok(Method::CONNECT),
            "OPTIONS" => Ok(Method::OPTIONS),
            "TRACE" => Ok(Method::TRACE),
            "PATCH" => Ok(Method::PATCH),
            _ => Ok(Method::UNKNOWN),

            // No error needed -> mapping to UNKNOWN
        }
    }
}
