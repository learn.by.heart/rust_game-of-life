use std::fmt::Display;

use super::StatusCode;

#[derive(Debug)]
pub struct Response {
    pub protocol: String,
    pub status_code: StatusCode,
    pub body: Option<String>,
}

impl Response {
    pub fn new(protocol: String, status_code: StatusCode, body: Option<String>) -> Self {
        Response { protocol, status_code, body }
    }
}

impl Display for Response {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {}", self.protocol, self.status_code, self.status_code.reason_phrase())
    }
}