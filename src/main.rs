mod model;
mod http;

use http::Server;

fn main() {
    println!("Hello, Game of Life! -> Open browser: 127.0.0.1:7878");

    let server = Server::new("127.0.0.1:7878".to_string());
    server.start();
}
